from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from .zad10.myproject.facebook.models import Facebook


class FacebookPlugin(CMSPluginBase):
    model = Facebook
    name = _("Wtyczka fejsbuk")
    render_template = "fb_plugin.html"

    def render(self, context, instance, placeholder):
        context['instance'] = instance
        return context

plugin_pool.register_plugin(FacebookPlugin)
