# -*- coding: utf-8 -*-

from cms.models.pluginmodel import CMSPlugin
from django.db import models


class Facebook(CMSPlugin):
	button = models.BooleanField()
	width = models.CharField(u'Width', max_length=50, default=u'450')
