from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from cms.models.pluginmodel import CMSPlugin

class Comment(models.Model):
	username = models.CharField(max_length=100)
	content = models.CharField(max_length=1000)
	date = models.DateTimeField(default=timezone.now())

	def __unicode__(self):
		return self.content

class Comments(CMSPlugin):
	comments = models.ManyToManyField(Comment, blank=True, null=True)

