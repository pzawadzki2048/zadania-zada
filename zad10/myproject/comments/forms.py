from django import forms

class CommentsForm(forms.Form):
    username = forms.CharField(max_length=100)
    content = forms.CharField(max_length=1000)
