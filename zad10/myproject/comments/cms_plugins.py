from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from zad10.myproject.comments.models import Comment, Comments
from zad10.myproject.comments.forms import CommentsForm

class CommentsPlugin(CMSPluginBase):
	model = Comments
	name = _("Komentarze")
	render_template = "comments.html"

	def render(self, context, instance, placeholder):
		request = context['request']
		if request.method == "POST":
			form = CommentsForm(request.POST)
			username = request.POST['username']
			content = request.POST['content']
			if form.is_valid():
				comment = Comment(username=username, content=content)
				comment.save()
				instance.comments.add(comment)

		context.update({
			'contact': instance,
			'form': CommentsForm(),
			'comments': instance.comments.all().order_by('-date'),
		})
		
		return context

plugin_pool.register_plugin(CommentsPlugin)
